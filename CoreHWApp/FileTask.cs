﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreHWApp
{
    public class FileTask
    {
        public int ReadFileOrCreateFileTaskOne(string value)
        {
            string path = @"D:\.net_projects\lesson4\cs04\CoreHWApp\Files\MyTest1.tsx";
            int result = 0;

            if (!File.Exists(path))
            {
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(value);
                }
            }

            using (StreamReader sr = File.OpenText(path))
            {
                string valueFile;
                while((valueFile = sr.ReadLine()) != null)
                {
                    result = NumberWordsLessThanFourChar(SplitString(valueFile,new char[] {' ',',', '.'}));
                }
            }

            return result;
        }

        public string ReadFileOrCreateFileTaskTwo(string value, int numberWords)
        {
            string path = @"D:\.net_projects\lesson4\cs04\CoreHWApp\Files\MyTest2.tsx";
            List<string> result = new List<string>();

            if (!File.Exists(path))
            {
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(value);
                }
            }

            using (StreamReader sr = File.OpenText(path))
            {
                string valueFile;
                while ((valueFile = sr.ReadLine()) != null)
                {
                    string[] sentences = SplitString(valueFile,new char[] {'.'});

                    foreach(string sentence in sentences)
                    {
                        string[] arrayWords = SplitString(sentence.Trim(), new char[] { ' ', ',' });
                        if (arrayWords.Length == numberWords) result.Add(string.Join(' ',arrayWords));
                    }
                }
            }

            return string.Join('.', result);
        }

        public int NumberWordsLessThanFourChar(string[] array)
        {
            int result = 0;
            foreach(string el in array)
            {
                if (el.Length < 4) result++;
            }

            return result;
        }

        public string[] SplitString(string value, char[] separators) =>
            value.Split(separators, StringSplitOptions.RemoveEmptyEntries);
    }
}
