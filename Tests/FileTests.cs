﻿using CoreHWApp;
using Xunit;

namespace Tests
{
    public class FileTests
    {
        [Fact]
        public void SplitString_SplitStringOnWords()
        {
            FileTask file = new FileTask();
            string[] result = file.SplitString("Hello. World, roman.", new char[] {' ', ',', '.'});
            Assert.Equal(new string[] { "Hello", "World", "roman" }, result);
        }

        [Fact]
        public void NumberWordsLessThanFourChar_WhenThereIsValueLessFour()
        {
            FileTask file = new FileTask();
            int result = file.NumberWordsLessThanFourChar(new string[] { "hel", "Roman", "Words", "Log" });
            Assert.Equal(2, result);
        }

        [Fact]
        public void ReadFileOrCreateFileTaskOne_WhetThereIsValue()
        {
            FileTask file = new FileTask();
            int result = file.ReadFileOrCreateFileTaskOne("hello rom, how are you");
            Assert.Equal(4, result);
        }

        [Fact]
        public void ReadFileOrCreateFileTaskTwo_WhetThereIsValue()
        {
            FileTask file = new FileTask();
            string result = file.ReadFileOrCreateFileTaskTwo("Hello world. My name's Roma. I'm developer", 2);
            Assert.Equal("Hello world.I'm developer", result);
        }
    }
}
